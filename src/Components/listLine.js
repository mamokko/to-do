import React from 'react';

const ListLine = (props) => {
    return (
        <div style={{display: 'flex', justifyContent: 'start', marginBottom: '10px'}}>
            <input className='checkbox' type='checkbox' onChange={props.onChange} />
            <label style={{marginRight: 'auto'}}>{props.text}</label>
            <button onClick={props.onDelete}>
                <i className="material-icons">
                    delete_outline
                </i>
            </button>
            <button style={{marginRight: '10px'}} onClick={props.onShowInput}>
                <i className="material-icons">
                    create
                </i>
            </button>
            {props.children}
        </div>
    )
};

export default ListLine;