import React from 'react';
import ListLine from "./listLine";
// import Modal from './modal';
class Everyday extends React.Component {
    state = {
        items: [
            {text: 'Почистить зубы'},
            {text:'Позвтракать'},
            {text:'Позвонить Аняше'},
            {text:'Заехать за Аняшей'},
            {text:'Не опоздать на работу'},
        ],
        showInput: false,
    };

    deleteHandler(index) {
      const items = this.state.items.concat();
      items.splice(index, 1);
      this.setState({items})
    };

    onShowInput = () => {
        this.setState({
            showInput: !this.state.showInput
        })
    };

    handleChange = () => {
        console.log('Change');
    };

    handleChangeText = (text, index) => {
        const item = this.state.item[index];
        item.text = text;
        const items = [...this.state.item];
        items[index] = items;
        this.setState({items});
    };

    render() {
        // const items = this.state.items;

        return (
            <div className="day">
                <h1 className="header">Список дел на 19.02.2019</h1>
                <div>
                    {this.state.items.map((items, index) => {
                        return(
                            <ListLine
                                key={index}
                                text={items.text}
                                onChange={this.handleChange}
                                onDelete={this.deleteHandler.bind(this, index)}
                                onShowInput={this.onShowInput}
                            >
                                {this.state.showInput
                                    ? <input
                                        type='text'
                                        defaultValue='Hello. its me'
                                        onChange={(event) => this.handleChangeText(event.target.value, index)}
                                    />
                                    : null
                                }

                            </ListLine>
                        )
                    })}
                </div>
                <div>
                    <h3>Комментарии</h3>
                    <textarea className='comment' placeholder='Мдаааааа....' />
                </div>
            </div>
        )
    }

}

export default Everyday;